require "faye"
require "eventmachine"
  
EM.run do
  client = Faye::Client.new('http://localhost:9292/faye')

  p "Started Clock"

  while true do
    client.publish '/foo', 'text' => Time.now
    sleep 15
  end
end